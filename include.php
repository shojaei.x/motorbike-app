<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 29/10/2018
 * Time: 08:30
 */

require_once 'config.php';

// Todo autoload
require_once 'classes/DataBase.php';
require_once 'classes/Storagable.php';
require_once 'classes/MotorBike.php';
require_once 'functions.php';
require_once 'classes/Image.php';
