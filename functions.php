<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 29/10/2018
 * Time: 08:36
 */


function save(Storagable &$object)
{
    $properties = $object->getDataBaseProperties();
    $table = $object->dataBaseName();

    if(method_exists($object,'saveToCache')){
        $object->saveToCache();
    }

    if (isset($properties['id'])) // update
    {
        $id = $properties['id'];
        unset($properties['id']);
        if (count($properties) >= 1) {
            DataBase::update_array($table, $properties, ['id=' . $id]);
        } else {
            return $object;
        }
    } else //  insert
    {
        $id = DataBase::insert_array($table, $properties);
        $object->id = $id;
    }

    if(method_exists($object,'syncOriginals')){
        $object->syncOriginals();
    }

    $object->saved();
    return $object;
}

function get_storagables(string $className, $column, $value)
{

    $object = new $className;
    $table = $object->dataBaseName();
    if (is_array($value)) {
        $value = json_encode($value);
    }
    $whereStr = $column . '=\'' . $value . '\'';

    $rows = DataBase::select_array($table, ['*'], [$whereStr]);

    $count = count($rows);
    if ($count) {
        $results = [];
        for ($i = 0; $i < $count; $i++) {
            $results[$i] = new $className;
            $results[$i]->fillProperties($rows[$i]);
        }
        return $results;
    }

    return null;
}

function get_motorbike_images_from_db($motorbike_id){
    return get_storagables('Image','motorbike_id',$motorbike_id);
}