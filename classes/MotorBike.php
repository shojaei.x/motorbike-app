<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 29/10/2018
 * Time: 08:34
 */

class MotorBike implements Storagable
{
    private $dataBaseColumns = [
        'id',
        'model',
        'color',
        'weight',
        'price',
        'created_at',
    ];
    private $tableName = 'motorbikes';
    function fillProperties(array $data)
    {

        foreach ($this->dataBaseColumns as $col) {
            if (isset($data[$col])) {
                $this->$col = $data[$col];
            }
        }
        unset($data['id']);
        $this->originals = $data;
    }
    function getDataBaseProperties(): array
    {
        $data = [];
        foreach ($this->dataBaseColumns as $col) {
            if (isset($this->$col)
                and (!isset($this->originals[$col]) or $this->originals[$col] != $this->$col)) {
                $data[$col] = $this->$col;
            }
        }
        if (isset($this->id)) {
            $data['id'] = $this->id;
        }
        return $data;
    }
    function dataBaseName(): string
    {
        return $this->tableName;
    }
    function saved(){}

    function save(){
        // save properties
        save($this);

        // save images | todo

    }

    function getImages() : array {
        // todo
    }

    function setModel(string $name){
        $this->model = $name;
        return $this;
    }
    function setColor(string $color){
        $this->color = $color;
        return $this;
    }
    function setWeight(int $weight){
        $this->weight = $weight;
        return $this;
    }
    function setPrice(int $price){
        $this->price = $price;
        return $this;
    }
    function setCreatedDate(DateTime $dateTime){
        $this->created_at = $dateTime->format('Y-m-d H:i:s');
        return $this;
    }
    function getCreatedDate() : DateTime {
        return DateTime::createFromFormat('Y-m-d H:i:s',$this->created_at);
    }

    function getAllProperties(): array
    {
        $data = [];
        foreach ($this->dataBaseColumns as $col) {
            if (isset($this->$col)) {
                $data[$col] = $this->$col;
            }
        }
        if (isset($this->id)) {
            $data['id'] = $this->id;
        }
        return $data;
    }
}