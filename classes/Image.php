<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 29/10/2018
 * Time: 08:51
 */

class Image implements Storagable
{
    private $dataBaseColumns = [
        'id',
        'motorbike_id',
        'file_name',
    ];
    private $tableName = 'images';
    function fillProperties(array $data)
    {

        foreach ($this->dataBaseColumns as $col) {
            if (isset($data[$col])) {
                $this->$col = $data[$col];
            }
        }
        unset($data['id']);
        $this->originals = $data;
    }
    function getDataBaseProperties(): array
    {
        $data = [];
        foreach ($this->dataBaseColumns as $col) {
            if (isset($this->$col)
                and (!isset($this->originals[$col]) or $this->originals[$col] != $this->$col)) {
                $data[$col] = $this->$col;
            }
        }
        if (isset($this->id)) {
            $data['id'] = $this->id;
        }
        return $data;
    }
    function dataBaseName(): string
    {
        return $this->tableName;
    }
    function saved(){}

    function save(){
        // save properties
    }
}