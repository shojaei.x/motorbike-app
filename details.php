<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 29/10/2018
 * Time: 08:27
 */

require_once 'include.php';

if(isset($_GET['id']) and is_numeric($_GET['id'])){

    // get motorbike object from db
    $id = $_GET['id'];
    $items = get_storagables('Motorbike','id',$id);
     if(count($items)){
         $motor_bike = $items[0];
         // print all motorbike properties
         foreach ($motor_bike->getAllProperties() as $key=>$value){
             echo $key . ' = ' . $value . '<br>';
         }
     } else {
         die('Motor bike ' . $id . ' not exitst.');
     }

     // get all motorbike images file name
     $file_names = [];
     $images = get_motorbike_images_from_db($id);
     foreach ($images as $image){
        array_push($file_names,$image->file_name);
     }
     //
     $width =  100/count($file_names);
     // show images
     foreach ($file_names as $file_name){
         echo '<img src="images/' . $file_name . '" width="' . $width . '%" >';
     }
}