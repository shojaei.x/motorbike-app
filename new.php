<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 29/10/2018
 * Time: 08:55
 */
if(isset($_POST['submit'])){

    require_once 'include.php';
    $errors = [];

    if(!(isset($_POST['price']) and is_numeric($_POST['price'])))
    {
        array_push($errors,'Invalid price value.');
    }

    if(!(isset($_POST['weight']) and is_numeric($_POST['weight'])))
    {
        array_push($errors,'Invalid weight value.');
    }

    if(!(isset($_POST['model']) and strlen($_POST['model'])<50))
    {
        array_push($errors,'Invalid model value.');
    }



    // check error arrays to be empty
    if(!count($errors)){
        // create new motor bike and save in database
        $new_motor_bike = new MotorBike();
        $new_motor_bike->setCreatedDate(new DateTime())
        ->setPrice($_POST['price'])
        ->setWeight($_POST['weight'])
        ->setModel(htmlspecialchars($_POST['model']))
        ->save();

        echo 'Motorbike number ' . $new_motor_bike->id . ' created successfuly.';

        // save images
        if(isset($new_motor_bike->id)) {
            for ($i = 0; $i < count($_FILES['filesToUpload']['tmp_name']); $i++) {

                // ignore none-image files
                $type = $_FILES['filesToUpload']['type'][$i];
                if (!in_array($type, ["image/jpeg", "image/png"])) {
                    continue;
                }

                // create new name for image with motorbike key
                $save_file_name = $new_motor_bike->id . '_' . $i . '.jpg';
                $path = 'images/' . $save_file_name;

                // move file from temp path to new path and save filename in database
                if (move_uploaded_file($_FILES['filesToUpload']['tmp_name'][$i], $path)) {
                    // save image in database
                    $image = new Image();
                    $image->motorbike_id = $new_motor_bike->id;
                    $image->file_name = $save_file_name;
                    save($image);
                }
            }
        }
        //
    }
    // show errors
    else {
        echo 'You have error in your inputs.' . '<br>' . implode('<br>',$errors);
    }
}
?>

<html>
<head>
    <title> Add new Motorbike</title>
</head>

<body>
    <form method="post" enctype="multipart/form-data" >
        Model: <input type="text" name="model" > <br>
        Weight: <input type="number" name="weight"> <br>
        Price: <input type="number" name="price" > <br>
        Select images: <input name="filesToUpload[]" id="filesToUpload" type="file" multiple="" />

        <input type="submit" name="submit" value="Add" >
    </form>
</body>
</html>
