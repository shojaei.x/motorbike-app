<?php

/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 14/11/2017
 * Time: 10:57
 */
class DataBase
{
    private static $connect;

    public static function isConnected()
    {
        return (self::$connect and @mysqli_ping(self::$connect));
    }

    private static function connect()
    {
        $connect = mysqli_connect(DataBaseHostName, DataBaseUserName, DataBasePassWord, DataBaseName);

        if ($connect) {
            $connect->set_charset("utf8");
            self::$connect = $connect;
            return $connect;
        } else {
            echo " <br> DB Not connected. <br>";
            self::sendError("DB Not connected. ");
            return false;
        }
    }

    public static function insert($table, $columns, $values)
    {

        self::connection();
        $q = "INSERT INTO $table ($columns) VALUES ($values)";

        $result = mysqli_query(self::$connect, $q);

        if ($result) {
            if (mysqli_insert_id(self::$connect) > 0) {
                return mysqli_insert_id(self::$connect);
            } else {
                return $result;
            }
        } else {
            self::sendError(mysqli_error(self::$connect) . "<br> \r\n 
        Query: $q \r\n ");
        }
    }


    public static function insert_array($table,$data)
    {

        self::connection();
        $columns =implode(',',array_keys($data));

        $values = '';
        $c = false;
        foreach (array_values($data) as $value){
            if($c) {
                $values .= ',';
            } else {
                $c = true;
            }
            $values .= '\'' . $value . '\'';

        }
        $q = "INSERT INTO $table ($columns) VALUES ($values)";

        $result = mysqli_query(self::$connect, $q);

        if ($result) {
            if (mysqli_insert_id(self::$connect) > 0) {
                return (int)mysqli_insert_id(self::$connect);
            } else {
                doLog('insert without id: ' . $q);
                return true;
            }
        } else {
            self::sendError(mysqli_error(self::$connect) . "<br> \r\n  " . $q);
            return false;
        }
    }

    public static function select($table, $columns, $where)
    {

        self::connection();
        $q = "SELECT $columns FROM $table WHERE $where";

        $result = mysqli_query(self::$connect, $q);
         if ($result) {
            return self::mysqli_fetch_all_array($result);
        } else {
            self::sendError(mysqli_error(self::$connect) . "\r\n" . $q);
        }

    }
    public static function select_array($table,array $columns, array $where = null)
    {

        self::connection();


        $columns = implode(',',$columns);

        $whereStr = ((is_null($where)) ? '1' : '');
        foreach($where as $condition){
            $whereStr.= ' ' . $condition;
        }


        $q = "SELECT $columns FROM $table WHERE $whereStr";

         $result = mysqli_query(self::$connect, $q);
         if ($result) {


            return self::mysqli_fetch_all_array($result);
        } else {
            self::sendError(mysqli_error(self::$connect) . "\r\n" . $q);
        }

    }
    public static function specialSelect($query)
    {

        self::connection();
        $result = mysqli_query(self::$connect, $query);
        //  sendError($query);
        if ($result) {
            return self::mysqli_fetch_all_array($result);
        } else {
            self::sendError(mysqli_error(self::$connect) . "</br> --specialSelect $query");
        }
    }

    public static function update($table, $set, $where)
    {

        self::connection();

        $query = "UPDATE $table SET $set WHERE $where";

        $result = mysqli_query(self::$connect, $query);

        if ($result) {
             return (int)mysqli_affected_rows(self::$connect);

        } else {
            $result = mysqli_query(self::$connect, $query);

            if ($result === false) {
                self::sendError(mysqli_error(self::$connect) . $query);
            }
            return false;
        }

    }
    public static function update_array($table,array $set, array $where = null){

        self::connection();

        $setStr = ((is_null($set)) ? '1' : '');
        $colon = false;
        foreach($set as $key=>$value){

            if($colon){
                $setStr .= ',';
            } else {
                $colon = true;
            }

            if(is_numeric($key)) {
               $setStr .= $value;
            } else {
                $value = str_replace('\'','',$value);
                $setStr .= $key . '=' . '\'' . $value . '\'';
            }
        }

        $whereStr = ((is_null($where)) ? '1' : '');
        foreach($where as $condition){
            $whereStr.= ' ' . $condition;
        }
         return self::update($table,$setStr,$whereStr);

    }

    public static function delete($table, $where)
    {

        self::connection();
        $query = "DELETE FROM $table WHERE $where ";

        $result = mysqli_query(self::$connect, $query);

        if ($result) {
            return $result;
        } else {
            self::sendError(mysqli_error(self::$connect) . "</br>DELETE table=\'$table\' where=\'$where\'");
        }
    }

    private static function mysqli_fetch_all_array(mysqli_result $mysqli_result)
    {
        return mysqli_fetch_all($mysqli_result,MYSQLI_ASSOC);
    }

    private static function sendError($text)
    {
          echo $text . "<br>";
    }

    public static function getTableFieldsArray($table_name)
    {
         $b = 0;
        $fields_array = array();
        foreach (self::mysqli_fetch_all_array(mysqli_query(self::$connect, "SHOW COLUMNS FROM $table_name")) as $item) {
             $fields_array[$b] = $item[0];
            $b++;
        }

        return $fields_array;
    }

    private static function connection()
    {
        if (self::isConnected()) {
            return;
        }
        self::connect();
        if (!self::isConnected()) {
            self::sendError("db connect error");
            throw new exception("DB not connected");
        }
    }

    public static function getConnection()
    {
        return self::$connect;
    }
}