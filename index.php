<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 29/10/2018
 * Time: 08:26
 */

require_once 'include.php';


$item_per_page = 5;
$min_id = 0;
// check for pagination and find last item id of previous page
if(isset($_GET['page']) and is_numeric($_GET['page']) and $_GET['page']>0){
    $page = $_GET['page'];
    // calc count of showed items in previous pages
    $last_item_number = ($page-1) * $item_per_page;

    $items = DataBase::select('motorbikes','id','1 LIMIT ' . $last_item_number);
    $count = count($items);
    if($count){
        $min_id = $items[$count-1]['id'];
    }
}
//

$order_string = '';
// check for sort
if(isset($_GET['sort']) and in_array($_GET['sort'],['date','price'])){
    $sort = $_GET['sort'];
    switch ($sort){
        case 'date': $order_column = 'created_at'; break;
        default: $order_column = $sort; break;
    }
    $order_string = 'Order by ' . $order_column;
}
//
$filter_string = '';
// check for color filtering
if(isset($_GET['color']) and is_string($_GET['color']) and strlen(htmlspecialchars($_GET['color']))){
    $color = htmlspecialchars($_GET['color']);
    $filter_string = ' and color=\'' . $color . '\'';
}
//
// select rows from database where item is greater than last item id of previous page
$rows = DataBase::select('motorbikes',"*","id>$min_id $filter_string $order_string Limit $item_per_page");


?>


<html>
<head>
    <title> All motorbikes</title>
</head>
<body>
<center>
    For Add new motorbike  <a href="new.php" >Click here </a>
    <hr>
<form>

    Page <input name="page" type="number" value="<?php echo ((isset($page)) ? $page : 1)?>">
    <br> Sort by:
    <input type="radio" id="sort1"
           name="sort" value="price" <?php if(isset($sort) and $sort=='price') echo 'checked'; ?> > Price
    <input type="radio" id="sort2"
           name="sort" value="date" <?php if(isset($sort) and $sort=='date') echo 'checked'; ?>> Date
    <br> Filter by color: <input type="text" name="color" <?php if(isset($color)) echo 'value="' . $color .'"'; ?>>
    <br>
    <input type="submit" value="show" >
</form>
    <hr>
<table border="1">
    <tr>
        <td>Id</td>
        <td>Model</td>
        <td>Color</td>
        <td>Weight</td>
        <td>Price</td>
        <td>Created date</td>
        <td>Details</td>
    </tr>
    <?php
    foreach ($rows as $row){

        echo '<tr>';
        echo '<td> ' . $row['id'] . '</td>';
        echo '<td> ' . $row['model'] . '</td>';
        echo '<td> ' . $row['color'] . '</td>';
        echo '<td> ' . $row['weight'] . '</td>';
        echo '<td> ' . $row['price'] . '</td>';
        echo '<td> ' . $row['created_at'] . '</td>';
        echo '<td> <a href="details.php?id=' .$row['id'] . '" target="_blank"> View </a></td>';
        echo '</tr>';
    }
    ?>
</table>
</center>
</body>
</html>
