<?php
/**
 * Created by PhpStorm.
 * User: Amin
 * Date: 2018-09-23
 * Time: 16:05
 */

Interface Storagable
{
    public function getDataBaseProperties() : array;

    public function fillProperties(array $properties);

    public function dataBaseName() : string;

    public function saved();

}